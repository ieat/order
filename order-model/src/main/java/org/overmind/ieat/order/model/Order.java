package org.overmind.ieat.order.model;

import lombok.Value;

@Value
public class Order {

    String id;

    Provider provider;

}
