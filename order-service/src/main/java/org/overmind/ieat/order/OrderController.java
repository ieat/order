package org.overmind.ieat.order;

import lombok.Value;
import org.overmind.ieat.order.model.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Value
@RestController
public class OrderController {

    OrderService orderService;

    @GetMapping
    public List<Order> findAll() {
        return orderService.findAll();
    }

}
