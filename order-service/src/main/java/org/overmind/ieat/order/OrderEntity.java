package org.overmind.ieat.order;

import lombok.Value;

@Value
public class OrderEntity {

    String id;

    String providerId;

}
