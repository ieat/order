package org.overmind.ieat.order;

import lombok.Value;
import org.overmind.ieat.order.model.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Value
@Service
public class OrderService {

    OrderRepository repository;

    List<Order> findAll() {
        return repository.findAll();
    }
}
