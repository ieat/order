package org.overmind.ieat.order;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("products")
public interface ProviderServiceClient {

    @GetMapping
    List<Provider> getProviders();

}
